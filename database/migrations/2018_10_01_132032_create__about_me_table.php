<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutMeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AboutMe', function (Blueprint $table) {
            $table->increments('personal_id');
            $table->string('PersonalName')->nullable();
            $table->string('PersonalInfomation')->nullable();
            $table->string('Image')->nullable();
            $table->string('YearExperience')->nullable();
            $table->string('DoneProject')->nullable();
            $table->string('HappyCustomer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AboutMe');
    }
}
