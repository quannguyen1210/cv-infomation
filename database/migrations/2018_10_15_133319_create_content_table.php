<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('content_id');
            $table->string('subtitle')->nullable($value=true);
            $table->string('subdetail')->nullable($value=true);
            $table->string('logo')->nullable($value=true);
            $table->string('date')->nullable($value=true);
            $table->string('description')->nullable($value=true);
            $table->string('skilltitle')->nullable($value=true);
            $table->string('rate')->nullable($value=true);
            $table->unsignedInteger('sub_personal_id');
            $table->foreign('sub_personal_id')->references('personal_id')->on('AboutMe');
            $table->unsignedInteger('sub_title_id');
            $table->foreign('sub_title_id')->references('title_id')->on('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
