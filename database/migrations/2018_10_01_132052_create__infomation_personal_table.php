<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfomationPersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('InfomationPersonal', function (Blueprint $table) {
            $table->increments('info_id');
            $table->string('key')->nullable();
            $table->string('value')->nullable();
            $table->string('description')->nullable();
            $table->unsignedInteger('pers_id');
            $table->foreign('pers_id')->references('personal_id')->on('AboutMe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('InfomationPersonal');
    }
}
