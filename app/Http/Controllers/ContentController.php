<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

Class ContentController extends Controller
{
	public function insert(Request $rq)
	{


		DB::table('content')->insert([

                'subtitle' => $rq->input('subtitle'),
                'subdetail' => $rq->input('subdetail'),
                'logo' => $rq->input('logo'),
                'date' => date("Y-m-d"),
                'description' => $rq->input('description'),
                'skilltitle' => $rq->input('skilltitle'),
                'rate' => $rq->input('rate'),
                'sub_personal_id' => $rq->input('sub_personal_id'),
                'sub_title_id' => $rq->input('sub_title_id')
        ]);
        

        return redirect('experience-index');
	}
	
    public function update(Request $rq)
    {
        $id = $rq->input('title_id');
        $title = $rq->input('title');
        $logo = $rq->input('logo');
        DB::table('content')->where('title_id',$id)->update([
            'title'=>$title,
            'logo' =>$logo,
        ]);

        return ;
    }

    public function delete(Request $rq)
    {
        $id = $rq->input('id');
        DB::table('content')->where('content_id',$id)->delete();
        return ;
    }
}