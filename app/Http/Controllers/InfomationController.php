<?php

namespace App\Http\Controllers;
use App\InfomationPersonal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InfomationController extends Controller
{
    public function showtable()
    {
        $aboutme = DB::table('aboutme')->get();
        $data = InfomationPersonal::all();
        return view('infomation',['data'=>$data,
                                'aboutme'=>$aboutme]
                    );
    }
    public function insert(Request $rq)
    {
        //get val to form
        $insert = new InfomationPersonal;
        $key = $_POST["key"];
        $value = $_POST["value"];
        $description = $_POST["description"];
        $perid= $rq->input["per_id"];
        //insert db
        $insert->key = "$key";
        $insert->value = "$value";
        $insert->description = "$description";
        $insert->pers_id ="$perid";
        $insert->save();

        return redirect('infomation');
    }
    public function delete(Request $rq)
    {
        $idp = $rq->input('id');

        DB::table('infomationpersonal')->where('info_id',$idp)->delete();

        return ;
    }

    public function update(Request $rq)
    {
        $id = $rq->input('info_id');
        $key = $rq->input('key');
        $value = $rq->input('value');
        $description = $rq->input('description');

        DB::table('infomationpersonal')->where('info_id',$id)->update([
            'key' => $key,
            'value' => $value,
            'description' => $description,
        ]);

        return ;
    }
}
