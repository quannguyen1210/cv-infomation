<?php

namespace App\Http\Controllers;

use App\AboutMe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutMeController extends Controller
{
    public function showtable()
    {
        $data = AboutMe::all();
        return view('aboutme',['data'=>$data]);
    }
    public function insert()
    {
        //get val to form
        $insert = new AboutMe;
        $persname= $_POST["personalname"];
        $info = $_POST["infomation"];
        $image = $_POST["image"];
        $yearexp = $_POST["yearexp"];
        $done = $_POST["done"];
        $happy = $_POST["happy"];
        //insert db
        $insert->PersonalName = "$persname";
        $insert->PersonalInfomation = "$info";
        $insert->Image = "$image";
        $insert->YearExperience = "$yearexp";
        $insert->DoneProject = "$done";
        $insert->HappyCustomer = "$happy";
        $insert->save();


        return redirect('aboutme');
    }
    public static function delete()
    {
        $ids = $_POST["id"];
        AboutMe::where('personal_id', $ids)->delete();
        return ;
    }
    public function update(Request $rq)
    {
        $id = $rq->input('id');
        $personalname = $rq->input('personalname');
        $perinfo = $rq->input('perinfo');
        $image = $rq->input('image');
        $experience = $rq->input('experience');
        $project = $rq->input('project');
        $customer = $rq->input('customer');

        DB::table('aboutme')->where('personal_id',$id)->update([
            'PersonalName' => $personalname,
            'PersonalInfomation' => $perinfo,
            'Image' => $image,
            'YearExperience' => $experience,
            'DoneProject' => $project,
            'HappyCustomer' => $customer,
        ]);

        return ;
    }
    // public function getdata()
    // {
    //    $ids = $_POST["id"];
    //    $datapop= AboutMe::where('personal_id', $ids)->toJson();
    //    return View::make('aboutme', [ 'datapop' => $datapop]);
        
    // }
}
