<?php

namespace App\Http\Controllers;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MyProfileController extends Controller
{
    public function showtable()
    {   
        $data = Menu::all();
        return view('menu',compact('data'));
    }
    public function show()
    {   
        $infomation = DB::table('infomationpersonal')->get();
        $aboutme = DB::table('aboutme')->get();
        $menu = DB::table('menu')->get();
        $title = DB::table('title')->get();
        $sub_personal_id = DB::table('aboutme')->pluck('personal_id')->first();

        $content = array();
         for ($i=0; $i < count($title) ; $i++) { 
            $val_title = $title[$i]->title_id;
          $content[] = DB::table('content')->where('sub_title_id',$val_title)
                               ->where('sub_personal_id',$sub_personal_id)->get();      
         }
               // echo "<pre>";
               // print_r($content);
               // echo "</pre>";

     return view('homepage',compact('infomation','aboutme','menu','content','title'));
     }
    public function insert()
    {
        //get val to form
        $insert = new Menu;
        $name = $_POST["name"];
        $hello = $_POST["hello"];
        $job = $_POST["job"];
        $textbg= $_POST["textbg"];
        $firsttitle= $_POST["firsttitle"];
        $lasttitle= $_POST["lasttitle"];
        //insert db
        $insert->name = "$name";
        $insert->hello = "$hello";
        $insert->job = "$job";
        $insert->textbg ="$textbg";
        $insert->firsttitle ="$firsttitle";
        $insert->lasttitle ="$lasttitle";
        $insert->save();
        return redirect('showmenu');
    }
    public function delete($id)
    {
       DB::table('menu')->where('id', $id)->delete();

        return redirect('showmenu');
    }
    public function update(Request $rq)
    {
         $id = $rq->input('id');
         $name = $rq->input('name');
         $hello = $rq->input('hello');
         $job = $rq->input('job');
         $textbg = $rq->input('textbg');

        DB::table('menu')->where('id',$id)->update([
             'name' => $name,
             'hello' => $hello,
             'job' => $job,
             'textbg' => $textbg
         ]);

         return true;
    }
}

