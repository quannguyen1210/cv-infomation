<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

Class ExperienceController extends Controller
{
	public function index()
	{
        $aboutme = DB::table('aboutme')->get();
        $content = DB::table('content')->get();
		$title = DB::table('title')->get();
        return view('experience',[
            'title'=>$title,
            'content'=>$content,
            'aboutme'=>$aboutme
            ]);
	}

	public function insert()
	{
		DB::table('title')->insert([

                'title' => $_POST['title'],
                'logo' => $_POST['logo']
        ]);
        

        return redirect('experience-index');
	}
	
    public function update(Request $rq)
    {
        $id = $rq->input('title_id');
        $title = $rq->input('title');
        $logo = $rq->input('logo');
        DB::table('title')->where('title_id',$id)->update([
            'title'=>$title,
            'logo' =>$logo,
        ]);

        return ;
    }

    public function delete(Request $rq)
    {
        $id = $rq->input('id');
        DB::table('title')->where('title_id',$id)->delete();
        return ;
    }

}
