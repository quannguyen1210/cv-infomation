<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Lina - Creative vCard, Resume, CV</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.animatedheadline.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/materialize.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/skins/yellow.css')}}" />

    <!-- Template JS Files -->
    <script src="{{asset('js/modernizr.custom.js')}}"></script>

</head>

<body class="dark">
    <!-- Preloader Start -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- Preloader Ends -->
    <!-- Wrapper Starts -->
    <div class="wrapper">
        <div id="bl-main" class="bl-main">
            <!-- Top Left Section Starts -->
            @for($i=0;$i< count($menu);$i++)
            @if(!empty($menu[$i]->name))
            <section class="topleft">
                <div class="bl-box row valign-wrapper">
                    <div class="row valign-wrapper">
                        <div class="title-heading">
                            <div class="selector uppercase" id="selector">
                                <h3 class="ah-headline p-none m-none">
                                    <span class="font-weight-300">{{$menu[$i]->hello}}</span>
                                    <span class="ah-words-wrapper">
                                        <b class="is-visible">{{$menu[$i]->name}}</b>
                                        @foreach($menu as $job)
                                        @if(!empty($job->job))
                                        <b>{{$job->job}}</b>
                                        @endif
                                        @endforeach
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Top Left Section Ends -->
            <!-- About Section Starts -->
            @else
            <section>
                <!-- About Title Starts -->

                <div class="bl-box valign-wrapper">
                    <div class="page-title center-align">
                        <span class="title-bg">{{$menu[$i]->textbg}}</span>
                        <h2 class="center-align">
                            <span data-hover="{{$menu[$i]->firsttitle}}">{{$menu[$i]->firsttitle}} </span>
                            <span data-hover="{{$menu[$i]->lasttitle}}">{{$menu[$i]->lasttitle}}</span>
                        </h2>
                    </div>
                </div>

                <div class="bl-content">
                    <!-- Main Heading Starts -->
                    <div class="container page-title center-align">
                        <h2 class="center-align">
                            <span data-hover="{{$menu[$i]->firsttitle}}">{{$menu[$i]->firsttitle}} </span>
                            <span data-hover="{{$menu[$i]->lasttitle}}">{{$menu[$i]->lasttitle}}</span>
                        </h2>
                        <span class="title-bg">{{$menu[$i]->textbg}}</span>
                    </div>
                    <!-- Main Heading Ends -->
                    <div class="container infos">
                        <!-- Divider Starts -->
                        <div class="divider center-align">
                            <span class="outer-line"></span>
                            <span class="fa fa-vcard" aria-hidden="true"></span>
                            <span class="outer-line"></span>
                        </div>
                        <!-- Divider Ends -->
                        <!-- Personal Informations Starts -->
                        <div class="row">
                            <!-- Picture Starts -->
                            <div class="col s12 m5 l4 xl3 profile-picture">
                                <img src="images/photo-about.jpg" class="responsive-img my-picture" alt="My Photo">
                            </div>
                            <!-- Picture Ends -->
                            <div class="col s12 m7 l8 xl9 personal-info">
                                <h6 class="uppercase"><i class="fa fa-user"></i> Personal Informations</h6>
                                <div class="col m12 l7 xl7 p-none">
                                    <p class="second-font">I'm a Freelance Web Designer & Developer based in Madrid, Spain.<br>
                                    I have serious passion for UI effects, animations and creating intuitive, 
                                    with over a decade of experience.
                                    </p>
                                </div>
                                @for($colum = 0;$colum < count($infomation);$colum++)
                                          @if($colum < count($infomation)/2) 
                                <div class="col s12 m12 l6 p-none">                                      
                                    <ul class="second-font list-1">
                                        <li><span class="font-weight-600">{{$infomation[$colum]->key}}: </span>{{$infomation[$colum]->value}}</li>
                                    </ul>
                                </div>
                                            @else
                                <div class="col s12 m12 l6 p-none">
                                    <ul class="second-font list-2"> 
                                        <li><span class="font-weight-600">{{$infomation[$colum]->key}}: </span>{{$infomation[$colum]->value}}</li>    
                                    </ul>    
                                </div>
                                            @endif
                                @endfor     
                                <a href="#" class="col s12 m12 l4 xl4 waves-effect waves-light btn font-weight-500">
                                    Download Resume <i class="fa fa-file-pdf-o"></i>
                                </a>
                                <a href="blog-dark.html" class="col s12 m12 l4 xl4 btn btn-blog font-weight-500">
                                    My Blog <i class="fa fa-edit"></i>
                                </a>
                            </div>
                        </div>
                        <!-- Personal Informations Ends -->
                    </div>
                    <!-- Resume Starts -->
                    <div class="resume-container">
                        <div class="container">
                            <div class="valign-wrapper row">
                                <!-- Resume Menu Starts --> 
                                <div class="resume-list col l4">
                                    @for($j = 0;$j < count($title);$j++)
                                    <div class="resume-list-item " data-index="{{$j}}" id="resume-list-item-{{$j}}">
                                        <div class="resume-list-item-inner">
                                            <h6 class="resume-list-item-title uppercase"><i class="fa fa-briefcase"></i> {{$title[$j]->title}}</h6>
                                        </div>
                                    </div>
                                    @endfor
                                </div>
                                <!-- Resume Menu Ends -->
                                <!-- Resume Content Starts -->
                                        <!-- Experience Starts -->
                                <div class="col s12 m12 l8 resume-cards-container">
                                    <div class="resume-cards">
                                        <!-- Experience Starts -->
                                        @for($numtit = 0;$numtit < count($title);$numtit++)
                                            @if($numtit < 2)
                                        <div class="resume-card resume-card-{{$numtit}}" data-index="{{$numtit}}">
                                            <!-- Experience Header Title Starts -->
                                            <div class="resume-card-header">
                                                <div class="resume-card-name"><i class="fa fa-briefcase"></i> {{$title[$numtit]->title}}</div>
                                            </div>
                                            <!-- Experience Header Title Ends -->
                                            <!-- Experience Content Starts -->
                                            <div class="resume-card-body experience">
                                                <div class="resume-card-body-container second-font">
                                                    <!-- Single Experience Starts -->
                                                    @for($numcont = 0;$numcont < count($content);$numcont++)
                                                        @for($subobj = 0; $subobj < count($content[$numcont]);$subobj++)
                                                            @if($title[$numtit]->title == "Experience" && $content[$numcont][$subobj]->sub_title_id == 0)
                                                    <div class="resume-content">
                                                        <h6 class="uppercase"><span>{{$content[$numcont][$subobj]->subtitle}} - </span>{{$content[$numcont][$subobj]->subdetail}}</h6>
                                                        <span class="date"><i class="fa fa-calendar-o"></i> {{$content[$numcont][$subobj]->date}}</span>
                                                        <p>{{$content[$numcont][$subobj]->description}}</p>
                                                    </div>
                                                    <span class="separator"></span>
                                                            @elseif($title[$numtit]->title == "Education" && $content[$numcont][$subobj]->sub_title_id == 1)
                                                    <div class="resume-content">
                                                        <h6 class="uppercase"><span>{{$content[$numcont][$subobj]->subtitle}} - </span>{{$content[$numcont][$subobj]->subdetail}}</h6>
                                                        <span class="date"><i class="fa fa-calendar-o"></i> {{$content[$numcont][$subobj]->date}}</span>
                                                        <p>{{$content[$numcont][$subobj]->description}}</p>
                                                    </div>  
                                                    <span class="separator"></span>
                                                            @endif
                                                        @endfor                       
                                                    @endfor
                                                    <!-- Single Experience Ends -->
                                                    <!-- Single Experience Starts -->
                                                   
                                                    <!-- Single Experience Ends -->
                                                    
                                                    <!-- Single Experience Starts -->
                                                    
                                                    <!-- Single Experience Ends -->
                                                </div>
                                            </div>
                                            <!-- Experience Content Starts -->
                                        </div>
                                        <!-- Experience Ends -->
                                        <!-- Education Starts -->
                                        <!-- Education Ends -->
                                        <!-- Skills Starts -->
                                            @else
                                        <div class="resume-card resume-card-{{$numtit}}" data-index="{{$numtit}}">
                                            <!-- Skills Header Title Starts -->
                                            <div class="resume-card-header">
                                                <div class="resume-card-name"><i class="fa fa-star"></i> {{$title[$numtit]->title}}</div>
                                            </div>
                                            <!-- Skills Header Title Starts -->
                                            <div class="resume-card-body skills">
                                                <div class="resume-card-body-container second-font">
                                                    <div class="row">
                                                        <!-- Skills Row Starts -->
                                                        <div class="col s6">
                                                            @for($numcont = 0 ; $numcont < count($content[$numtit]);$numcont++)
                                                            <!-- Single Skills Starts -->
                                                                @if($numcont < 6)
                                                            <div class="resume-content">
                                                                <h6 class="uppercase">{{$content[$numtit][$numcont]->skilltitle}} </h6>
                                                                <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></p>
                                                            </div>
                                                                @endif
                                                            @endfor
                                                        </div>
                                                        <!-- Skills Row Ends -->
                                                        <!-- Skills Row Starts -->                     
                                                        <div class="col s6">
                                                            @for($numcont1 = 6 ;$numcont1 < count($content[$numtit]);$numcont1++)
                                                            <!-- Single Skills Starts -->
                                                                @if($numcont1 < 12)
                                                            <div class="resume-content">
                                                                <h6 class="uppercase">{{$content[$numtit][$numcont1]->skilltitle}}</h6>
                                                                <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-empty"></i></p>
                                                            </div>
                                                                @endif
                                                            @endfor                                                            
                                                        </div>       
                                                        <!-- Skills Row Ends -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            @endif
                                        @endfor
                                        <!-- Skills Ends -->
                                    </div>
                                </div>                                                   


                                        <!-- Experience Ends -->
                                        <!-- Education Starts -->

                                        <!-- Education Ends -->
                                        <!-- Skills Starts -->

                                        <!-- Skills Ends -->
                                <!-- Resume Content Ends -->
                            </div>
                        </div>
                    </div>
                    <!-- Resume Ends -->
                    <!-- Fun Facts Starts -->
                    <div class="container badges">
                        <div class="row">
                            <!-- Fact Badge Item Starts -->
                            <div class="col s12 m4 l4 center-align">
                                <h3>
                                    <i class="fa fa-suitcase"></i>
                                    <span class="font-weight-700">7+</span>
                                </h3>
                                <h6 class="uppercase font-weight-500">Years Experience</h6>
                            </div>
                            <!-- Fact Badge Item Ends -->
                            <!-- Fact Badge Item Starts -->
                            <div class="col s12 m4 l4 center-align">
                                <h3>
                                    <i class="fa fa-check-square"></i>
                                    <span class="font-weight-700">89+</span>
                                </h3>
                                <h6 class="uppercase font-weight-500">Done Projects</h6>
                            </div>
                            <!-- Fact Badge Item Ends -->
                            <!-- Fact Badge Item Starts -->
                            <div class="col s12 m4 l4 center-align">
                                 <h3>
                                    <i class="fa fa-heart"></i>
                                    <span class="font-weight-700">77+</span>
                                </h3>
                                <h6 class="uppercase font-weight-500">Happy customers</h6>
                            </div>
                            <!-- Fact Badge Item Ends -->
                        </div>
                    </div>
                    <!-- Fun Facts Ends -->
                </div>
                    <!-- Resume Ends -->
                    <!-- Fun Facts Starts -->
                    
                <!-- End Expanded About -->
                <img alt="close" src="images/close-button.png" class="bl-icon-close" />
            </section>
            @endif
            @endfor
            <!-- About Ends -->
            <!-- Portfolio Starts -->
            
            <!-- Portfolio Section Ends -->
            <!-- Contact Section Starts -->

            <!-- Contact Section Ends -->
            <!-- Portfolio Panel Items Starts -->

            <!-- Portfolio Panel Items Ends -->
        </div>
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
    <script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('js/jquery.animatedheadline.js')}}"></script>
    <script src="{{asset('js/boxlayout.js')}}"></script>
    <script src="{{asset('js/materialize.min.js')}}"></script>
    <script src="{{asset('js/jquery.hoverdir.js')}}"></script>
    <script src="{{asset('js/custom.js')}}"></script>
</body>

</html>