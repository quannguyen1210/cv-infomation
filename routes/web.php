<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin','AdminController@ShowIndex')->name('admin');


//route aboutme
Route::post('delete-ab','AboutMeController@delete')->name('delete-ab');
Route::post('insert-ab','AboutMeController@insert')->name('insert-ab');
Route::get('aboutme','AboutMeController@showtable')->name('showtable');
Route::post('update-ab','AboutMeController@update')->name('update-ab');
//end

//route Infomation personal
Route::post('delete-info','InfomationController@delete')->name('delete-info');
Route::post('insert-info','InfomationController@insert')->name('insert-info');
Route::post('update-info','InfomationController@update')->name('update-info');
Route::get('infomation','InfomationController@showtable')->name('showtable-info');
//end

//Route Myprofile
    //route menu table
Route::get('showmenu','MyProfileController@showtable')->name('showmenu');
Route::get('showindex','MyProfileController@show')->name('showProfile');
Route::get('delete-menu/{id}','MyProfileController@delete')->name('delete-menu');
Route::post('update-menu','MyProfileController@update')->name('update-menu');
Route::post('insert-menu','MyProfileController@insert')->name('insert-menu');

//Experience
Route::post('insert-exp','ExperienceController@insert')->name('insert-exp');
Route::post('update-exp','ExperienceController@update')->name('update-exp');
Route::post('delete-exp','ExperienceController@delete')->name('delete-exp');
Route::get('experience-index','ExperienceController@index')->name('experience-index');/// + content index

//content
Route::post('insert-cont','ContentController@insert')->name('insert-cont');
Route::post('update-cont','ContentController@update')->name('update-cont');
Route::post('delete-cont','ContentController@delete')->name('delete-cont');

//upload
 Route::post('upload','UploadController@upload');
 Route::get('upload-index','UploadController@index');

 Route::get('baitap','UploadController@baitap');

Route::get('test',function(){
    return view('popup');
});