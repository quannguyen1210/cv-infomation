$(function () {
    $('#example1 .btn_Delete' ).click(function(){
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "delete-exp",
        type: "POST",
        data: {'id': id},
        contentType: "application/x-www-form-urlencoded",
        success :function () {
           alert('Delete success');
           window.location('delete-exp');
        }
      })
    });
})

$('#example1  .update-pop' ).click(function(){
        var csrf = $('meta[name="csrf-token"]').attr('content');
        var id = $(this).attr('id');
        var title = $('#title_'+id).val();
        var logo = $('#logo_'+id).val();
       $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        contentType: "application/x-www-form-urlencoded",
        url: "update-exp",
        data: {"title_id": id,
                "title": title,
                "logo": logo
            },
        success: function(){
            alert("Update Success");
            location.reload();
           
        }
        });
    });


