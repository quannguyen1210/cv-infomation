$(function () {
    var table = $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    });
    $('#example1 .btn_Delete' ).click(function(){
        var id = $(this).val();
        var csrf = $('meta[name="csrf-toke  n"]').attr('content');
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "delete-ab",
        type: "POST",
        data: {'id': id},
        contentType: "application/x-www-form-urlencoded",
        success :function (data) {
            window.location = "aboutme";
        }
      })
    });
})
//edit pop-up
   $('#example1 tr td a').click(function(){
        var id =$(this).attr('id');
        var pers = $("#personalname"+id).html();
        var persinfo = $("#personalinfomation"+ id).html();
        var year = $("#year"+ id).html();
        var image = $("#ima"+ id).html();
        var doneproj = $("#doneproject"+ id).html();
        var hpcustomer = $("#happycustomer"+ id).html();
        $('#update-pop').val(id);
        $('#pername-pop').attr("placeholder",pers );;
        $('#infomation-pop').attr("placeholder",persinfo);
        $('#image-pop').attr("placeholder",year);
        $('#experience-pop').attr("placeholder",image);
        $('#project-pop').attr("placeholder",doneproj);
        $('#customer-pop').attr("placeholder",hpcustomer);
        
    });
    //end edit pop-up
    // $('.data-pop #update-pop').click(function(){
    //     var ids =$('#id-pop').val();
    //     var pernames= $('#pername-pop').val();
    //     var infors= $('#infomation-pop').val();
    //     var images= $('#image-pop').val();
    //     var expers= $('#experience-pop').val();
    //     var projs= $('#project-pop').val();
    //     var customs= $('#customer-pop').val();
    //     $.post('/update-ab',
    //         {
    //             id: ids,
    //             pername:pernames,
    //             infor:infors,
    //             image:images,
    //             exper:expers,
    //             proj:projs,
    //             custom:customs
    //         }
    //     )
    // });
    $('.data-pop  #update-pop' ).click(function(){
        var csrf = $('meta[name="csrf-token"]').attr('content');
        var id = $(this).val();
        var personalname = $('#pername-pop').val() ;
        var perinfo = $('#infomation-pop').val();
        var image = $('#image-pop').val();
        var experience = $('#experience-pop').val();
        var project = $('#project-pop').val();
        var customer = $('#customer-pop').val();
       $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        contentType: "application/x-www-form-urlencoded",
        url: "update-ab",
        data: {"id": id,
                "personalname": personalname,
                "perinfo": perinfo,
                "image": image,
                "experience": experience,
                "project": project,
                "customer": customer
            },
        success: function(){
            alert("Update Success");
           
        }
        });
    });    
    

